package calc;

import java.util.Scanner;

import org.apache.log4j.Logger;

class Calculator {
	private static final Logger log = Logger.getLogger(Calculator.class);

	public static void main(String[] args) {

		char operator;
		Double number1, number2, result;

		// create an object of Scanner class
		Scanner input = new Scanner(System.in);

		// ask users to enter operator
		log.info("Choose an operator: +, -, *, or /");
		operator = input.next().charAt(0);

		// ask users to enter numbers
		log.info("Enter first number");
		number1 = input.nextDouble();

		log.info("Enter second number");
		number2 = input.nextDouble();

		switch (operator) {

		// performs addition between numbers
		case '+':
			result = number1 + number2;
			log.info(number1 + " + " + number2 + " = " + result);
			break;

		// performs subtraction between numbers
		case '-':
			result = number1 - number2;
			log.info(number1 + " - " + number2 + " = " + result);
			break;

		// performs multiplication between numbers
		case '*':
			result = number1 * number2;
			log.info(number1 + " * " + number2 + " = " + result);
			break;

		// performs division between numbers
		case '/':
			result = number1 / number2;
			log.info(number1 + " / " + number2 + " = " + result);
			break;

		default:
			log.info("Invalid operator!");
			break;
		}

		input.close();
	}
}
