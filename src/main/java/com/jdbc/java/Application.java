package com.jdbc.java;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

	public static void main(String[] args) {
		ApplicationContext contex = new AnnotationConfigApplicationContext("user-beans.xml");

		AutowireCapableBeanFactory autowireCapableBeanFactory = contex.getAutowireCapableBeanFactory();
	}

}
