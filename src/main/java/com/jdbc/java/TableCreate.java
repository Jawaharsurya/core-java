package com.jdbc.java;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class TableCreate {
	private static final Logger log = Logger.getLogger(TableCreate.class);

	private static final String SQL_CREATE = "CREATE TABLE players" + "(" + " ID serial,"
			+ " NAME varchar(100) NOT NULL," + " SALARY numeric(15, 2) NOT NULL,"
			+ " CREATED_DATE timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP," + " PRIMARY KEY (ID)" + ")";

	public static void main(String[] args) {

		try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres",
				"1234"); PreparedStatement preparedStatement = conn.prepareStatement(SQL_CREATE)) {

			preparedStatement.execute();
			System.out.println("created table Sucessful");

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
