package com.collectionsandjava8.java;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

public class Application {
	private static final Logger log = Logger.getLogger(Application.class);

	public static void main(String[] args) {

		List<Student> studentList = new ArrayList<Student>();
		studentList.add(new Student(1, "jay", 18));
		studentList.add(new Student(2, "sai", 15));
		studentList.add(new Student(3, "ram", 17));
		studentList.add(new Student(4, "Bajrang", 14));
		studentList.add(new Student(5, "Abhi", 10));
		studentList.add(new Student(4, "Bajrang", 14));
		studentList.add(new Student(10, "Bajrang", 19));
		studentList.add(new Student(9, "ram", 17));

		String list = studentList.stream().map(x -> x.name.toUpperCase()).distinct().collect(Collectors.joining(","));
		log.info("distinct names of students in uppercase : " + list);

		List<Student> age = studentList.stream().filter(x -> x.age > 18).collect(Collectors.toList());
		log.info(age);

		Double avgAge = studentList.stream().collect(Collectors.averagingInt(p -> p.age));
		log.info("average age : " + avgAge);
	}

}
