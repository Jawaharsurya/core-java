package com.collectionsandjava8.java;

import java.util.HashMap;

import org.apache.log4j.Logger;

public class CollectionProgram {
	private static final Logger log = Logger.getLogger(CollectionProgram.class);

	public static void main(String[] args) {

		String str = "abababc";

		HashMap<Character, Integer> mapCount = new HashMap<>();

		for (int i = str.length() - 1; i >= 0; i--) {
			if (mapCount.containsKey(str.charAt(i))) {
				int count = mapCount.get(str.charAt(i));
				mapCount.put(str.charAt(i), ++count);
			} else {
				mapCount.put(str.charAt(i), 1);
			}
		}

		log.info(mapCount);
	}

}
