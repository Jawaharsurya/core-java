package com.collectionsandjava8.java;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

public class App {
	private static final Logger log = Logger.getLogger(App.class);

	public static void main(String[] args) {
		List<Product> proList = new ArrayList<Product>();

		proList.add(new Product(1, "Pen", 20.05));
		proList.add(new Product(2, "Bat", 1000.11));
		proList.add(new Product(3, "Ball", 100.90));
		proList.add(new Product(4, "Phone", 20000.99));
		proList.add(new Product(5, "Teddy", 1000.01));

		List<Product> leastPrice = proList.stream().min(Comparator.comparing(Product::getProductPrice)).stream()
				.collect(Collectors.toList());
		log.info("the least product price : " + leastPrice);
		List<Product> maxPrice = proList.stream()

				.max(Comparator.comparing(Product::getProductPrice)).stream().collect(Collectors.toList());
		log.info("the maximum product price : " + maxPrice);

	}

}

class Product {
	int productId;
	String productName;
	double productPrice;

	public Product(int productId, String productName, double productPrice) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productPrice = productPrice;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice
				+ "]";
	}

}
