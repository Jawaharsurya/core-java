package com.pojo.java;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class App {
	private static final Logger log = Logger.getLogger(App.class);

	public static void main(String[] args) {

		ApplicationContext context = new GenericXmlApplicationContext("bean.xml");
		Data data = (Data) context.getBean("data");
		log.info(data.getDeptname());
		log.info(data.getName());
		log.info(data.getAge());

		log.info(data.toString());
	}

}
