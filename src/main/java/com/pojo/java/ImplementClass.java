package com.pojo.java;

import java.util.Scanner;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class ImplementClass implements Execute {
	private static final Logger log = Logger.getLogger(ImplementClass.class);

	double rupees;
	double dollars;

	@Override
	public void conversion() {

		Scanner in = new Scanner(System.in);

		log.info("Please enter rupees:");

		rupees = in.nextLong();
		dollars = rupees / 75.61;

		log.info(dollars + " Dollars");
		in.close();
	}

	public static void main(String[] args) {
		ApplicationContext context = new GenericXmlApplicationContext("services.xml");
		context.getBean("cal");

	}
}
