package com.pojo.java;

public class Data {
	private String deptname;
	private String name;
	private int age;

	@Override
	public String toString() {
		return "Data [deptname=" + deptname + ", name=" + name + ", age=" + age + "]";
	}

	public String getDeptname() {
		return deptname;
	}

	public void setDeptname(String deptname) {
		this.deptname = deptname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
