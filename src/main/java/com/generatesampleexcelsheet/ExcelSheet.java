package com.generatesampleexcelsheet;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExcelSheet {
	private static final Logger log = Logger.getLogger(ExcelSheet.class);

	public static void main(String[] args) throws IOException {

		FileOutputStream fileOut;

		String outPut = "Employee.xls";

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Employee");

		fileOut = new FileOutputStream(outPut);
		workbook.write(fileOut);
		fileOut.close();
		workbook.close();
		log.info("created sheet");

	}

}
