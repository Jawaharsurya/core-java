package com.javatraining;

import org.apache.log4j.Logger;

class Machine {
	private static final Logger log = Logger.getLogger(Machine.class);

	public void start() {
		log.info("machine starts");
	}

	public void Stop() {
		log.info("machine stops");
	}
}

class Bike extends Machine {
	private static final Logger log = Logger.getLogger(Bike.class);

	public void start() {
		log.info("bike start");
	}

	public void breakk() {
		log.info("press breaks");
	}

}

public class InheritanceAndOverriding {

	public static void main(String[] args) {
		Bike b1 = new Bike();
		b1.start();
		b1.breakk();
		b1.Stop();

	}

}
