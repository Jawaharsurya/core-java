package com.javatraining;

import org.apache.log4j.Logger;

public abstract class Abst {
	abstract void run();

}

class Human extends Abst {
	private static final Logger log = Logger.getLogger(Human.class);

	@Override
	void run() {
		log.info("run 5 kms");

	}

}