package com.javatraining;

import org.apache.log4j.Logger;

public interface Automobile {
	void start();

}

class Car implements Automobile {
	private static final Logger log = Logger.getLogger(Car.class);

	public void start() {
		log.info("car has started");

	}
}
