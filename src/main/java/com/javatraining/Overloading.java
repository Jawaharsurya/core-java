package com.javatraining;

import org.apache.log4j.Logger;

public class Overloading {
	private static final Logger log = Logger.getLogger(Overloading.class);

	public void start(int a) {
		log.info("method with single argument is triggered");
	}

	public void start(double a) {
		log.info("method with double argument triggered");
	}

	public static void main(String[] args) {
		Overloading o = new Overloading();
		o.start(1);
		o.start(1.1);
	}
}
