package com.javatraining;

import org.apache.log4j.Logger;

public class Main {
	private static final Logger log = Logger.getLogger(Main.class);

	public static void main(String[] args) {

		Encapsulation e = new Encapsulation();
		e.setName("sai");
		e.setAge(20);
		log.info("my name is : " + e.getName() + " my age is :" + e.getAge());
		Human h = new Human();
		h.run();
		Car c = new Car();
		c.start();
	}
}
